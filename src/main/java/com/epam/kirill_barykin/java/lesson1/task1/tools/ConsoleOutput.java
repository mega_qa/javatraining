package com.epam.kirill_barykin.java.lesson1.task1.tools;

import java.util.Arrays;

/**
 * Вывод значений на консоль
 */

public class ConsoleOutput {

    /**
     * Вывод массива в консоль
     *
     * @param array   - исходный массив
     * @param subTask - номер задания
     */

    public void consoleArrayOutput(int[] array, int subTask) {

        String message;

        switch (subTask) {
            case (1):
                message = "(" + subTask + "). " + "Swapped the maximum negative element and the minimum positive: ";
                break;
            case (3):
                message = "(" + subTask + "). " + "Array with negative numbers replaced by zeros: ";
                break;
            case (4):
                message = "(" + subTask + "). " + "Array with tripled positive elements before negative ones: ";
                break;
            case (6):
                if (array.length > 1) {
                    message = "(" + subTask + "). " + "Elements are repeated and have an odd index: ";
                } else {
                    message = "(" + subTask + "). " + "There is no duplicates :(";
                }
                break;

            default:
                throw new IllegalStateException("Unexpected value: " + subTask);
        }

        System.out.print(message);
        if (array.length > 1) {
            System.out.println(Arrays.toString(array));
        } else System.out.print("");

    }

    /**
     * Вывод числа в консоль
     *
     * @param sum     - число, которое необходимо отобразить в консоли
     * @param subTask - номер задания
     */

    public void consoleSumOutput(double sum, int subTask) {

        String message = null;

        switch (subTask) {
            case (2):
                message = "(" + subTask + "). " + "Sum of elements in even positions: ";
                break;
            case (5):
                message = "(" + subTask + "). " + "The difference between the average and the minimum value: ";
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + subTask);
        }

        System.out.println(message + sum);

    }
}
