package com.epam.kirill_barykin.java.lesson1.task1;

import com.epam.kirill_barykin.java.lesson1.task1.functional.ArrayManipulations;
import com.epam.kirill_barykin.java.lesson1.task1.functional.DuplicatesWithOddIndexes;
import com.epam.kirill_barykin.java.lesson1.task1.functional.SwapMaxNegAndMinPos;
import com.epam.kirill_barykin.java.lesson1.task1.tools.ConsoleOutput;

/**
 * Author: Kirill Barykin
 * Task №2
 * Написать программу, осуществляющую обработку массива. Исходные
 * условия: массив содержит только целые числа от -10 до 10 (допускается
 * генерация элементов с помощью метода (int) Math.random()), программа
 * должна выводить в консоль исходный массив и полученный результат,
 * количество элементов в массиве 20.
 * Варианты задания:
 * 1. В массиве целых чисел поменять местами максимальный
 * отрицательный элемент и минимальный положительный.
 * 2. В массиве целых чисел определить сумму элементов, состоящих на
 * чётных позициях.
 * 3. В массиве целых чисел заменить нулями отрицательные элементы.
 * 4. В массиве целых чисел утроить каждый положительный элемент,
 * который стоит перед отрицательным.
 * 5. В массиве целых чисел найти разницу между средним
 * арифметическим и значение минимального элемента.
 * 6. В массиве целых чисел вывести все элементы, которые встречаются
 * больше одного раза и индексы которых нечётные.
 *
 */
public class ArrayProcessingApplication {
    public static void main(String[] args) {
        ArrayProcessingApplication arrayProcessingApplication = new ArrayProcessingApplication();

        arrayProcessingApplication.startApplication();

    }

    private void startApplication() {
        SwapMaxNegAndMinPos swapMaxNegAndMinPos = new SwapMaxNegAndMinPos();
        ArrayManipulations arrayManipulations = new ArrayManipulations();
        DuplicatesWithOddIndexes duplicatesWithOddIndexes = new DuplicatesWithOddIndexes();
        ConsoleOutput console = new ConsoleOutput();

        int[] coreArray = arrayManipulations.generate();

        // console.consoleArrayOutput(swapMaxNegAndMinPos.swap(coreArray), 1);
        console.consoleArrayOutput(swapMaxNegAndMinPos.swapItEasy(coreArray), 1);
        console.consoleSumOutput(arrayManipulations.evenNumbersSum(coreArray), 2);
        console.consoleArrayOutput(arrayManipulations.replaceNegativeValuesByZeros(coreArray), 3);
        console.consoleArrayOutput(arrayManipulations.triplePositiveValuesBeforeNegative(coreArray), 4);
        console.consoleSumOutput(arrayManipulations.differenceBetweenAverageAndMinimalValueEasy(coreArray), 5);
        console.consoleArrayOutput(duplicatesWithOddIndexes.duplicatesWithOddIndexes(coreArray), 6);
        //  console.consoleArrayOutput(duplicatesWithOddIndexes.duplicatesWithOddIndexesHashSet(coreArray), 6);
    }
}
