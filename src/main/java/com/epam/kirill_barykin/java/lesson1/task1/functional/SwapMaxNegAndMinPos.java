package com.epam.kirill_barykin.java.lesson1.task1.functional;

/**
 * Author: Kirill Barykin
 * Task №1
 * Поменять местами максимальный отрицательный элемент и минимальный положительный
 */


public class SwapMaxNegAndMinPos {
    private ArrayManipulations arrayManipulations = new ArrayManipulations();

    /**
     * Поменять местами максимальный отрицательный элемент и минимальный положительный
     *
     * @param coreArray - исходный массив
     * @return видоизменённый массив
     */

    public int[] swap(int[] coreArray) {

        int[] swappedArray = new int[coreArray.length];

        for (int i = 0; i < coreArray.length; i++) {
            swappedArray[i] = coreArray[i];
        }

        int[] posArr = arrayManipulations.positiveArray(coreArray);
        int[] negArr = arrayManipulations.negativeArray(coreArray);

        int maximalNegative = negArr[negArr.length - 1];
        int minimalPositive = posArr[0];

        int maximalNegativePosition = 0;
        int minimalPositivePosition = 0;

        for (int i = swappedArray.length - 1; i > 0; i--) {
            if (swappedArray[i] == maximalNegative) {
                maximalNegativePosition = i;
            } else if (swappedArray[i] == minimalPositive) {
                minimalPositivePosition = i;
            }
        }

        int swapped = swappedArray[maximalNegativePosition];
        swappedArray[maximalNegativePosition] = swappedArray[minimalPositivePosition];
        swappedArray[minimalPositivePosition] = swapped;

        return swappedArray;
    }

    /**
     * Поменять местами максимальный отрицательный элемент и минимальный положительный (более оптимизированный, но менее весёлый метод)
     *
     * @param coreArray - исходный массив
     * @return видоизменённый массив
     */
    public int[] swapItEasy(int[] coreArray) {

        int length = coreArray.length;
        int biggestNegative = 0;
        int smallestPositive = 0;

        int[] swappedArray = new int[length];

        for (int i = 0; i < length; i++) {
            swappedArray[i] = coreArray[i];
        }

        for (int i = 0; i < length; i++) {
            if (swappedArray[i] < 0) {
                if (swappedArray[i] > swappedArray[biggestNegative]){
                    biggestNegative = i;
                }
                if (swappedArray[biggestNegative] >= 0){
                    biggestNegative = i;
                }
            }else if (swappedArray[i] > 0) {
                if (swappedArray[i] < swappedArray[smallestPositive])
                    smallestPositive = i;
                if (swappedArray[smallestPositive] <= 0)
                    smallestPositive = i;
            }
        }

        int biggestValue = swappedArray[biggestNegative];
        swappedArray[biggestNegative] = swappedArray[smallestPositive];
        swappedArray[smallestPositive] = biggestValue;

        return swappedArray;
    }

}

