package com.epam.kirill_barykin.java.lesson1.task2.tools;

/**
 * Author: Kirill Barykin
 * Task №2
 * Вывод значений в консоль
 */

public class ConsoleOutput {

    public void subTaskNumber(int subTask){
        System.out.println("\n...SubTask №" + subTask + "...\n");
    }

}
