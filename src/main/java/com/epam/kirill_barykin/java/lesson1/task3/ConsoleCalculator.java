package com.epam.kirill_barykin.java.lesson1.task3;

import java.util.Scanner;

/**
 * Author: Kirill Barykin
 * Task №3
 * Написать калькулятор – программа, которая на входе получает два целых
 * числа (число А и число В), выполняет арифметическую операцию («+» -
 * сложение, «-» - вычитание, «*» - умножение, «/» - деление) и выводит результат в
 * консоль. Для каждой операции использовать отдельный метод (нельзя
 * использовать методы библиотечного класса Math). Предусмотреть
 * пользовательское меню для выбора необходимой операции. Ввод входных данных
 * осуществлять с клавиатуры.
 */

public class ConsoleCalculator {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        ConsoleCalculator consoleCalculator = new ConsoleCalculator();

        consoleCalculator.startApplication();
    }

    /**
     * Запуск приложения
     */
    private void startApplication() {
        int firstValue = inputValue();
        int secondValue = inputValue();
        int operationValue = operationValue();
        calculate(firstValue, secondValue, operationValue);
    }

    /**
     * Ввод значений
     *
     * @return введённое значение
     */
    private int inputValue() {
        int usersValue;
        boolean isItNumber = false;

        while (!isItNumber) {
            System.out.print("Insert the number:");
            if (scanner.hasNextInt()) {
                isItNumber = true;
            } else {
                System.out.println("It is not a number. Try again.");
                scanner.next();
            }
        }
        usersValue = scanner.nextInt();
        return usersValue;
    }

    /**
     * Выбор производимой расчётной операции с введёнными числами
     *
     * @return символ расчётной операции с числами
     */
    private static int operationValue() {
        int operationValue;

        System.out.print("Select operation with numbers\n1 - \"+\"\n2 - \"-\"\n3 - \"/\"\n4 - \"*\"\nYour choice is: ");
        if (scanner.hasNextInt()) {
            operationValue = scanner.nextInt();
        } else {
            System.out.println("It is not a number. Try again.");
            scanner.next();
            operationValue = operationValue();
        }


        return operationValue;
    }

    /**
     * Выполнение расчётов над числами
     *
     * @param firstValue     - первое введённое значение
     * @param secondValue    - второе введённое значение
     * @param operationValue - символ выполняемой операции
     * @return результат
     */
    private void calculate(int firstValue, int secondValue, int operationValue) {
        switch (operationValue) {
            case 1: // "+"
                calculateSum(firstValue, secondValue);
                break;
            case 2: // "-"
                calculateDifference(firstValue, secondValue);
                break;
            case 3: // "/"
                calculateQuotient(firstValue, secondValue);
                break;
            case 4: // "*"
                calculateProduct(firstValue, secondValue);
                break;
            default:
                System.out.println("You entered wrong value. Try again");
                calculate(firstValue, secondValue, operationValue());
        }
    }

    /**
     * Метод подсчёта суммы двух чисел
     *
     * @param firstValue  - первое значение
     * @param secondValue - второе значение
     * @return - результат
     */
    private void calculateSum(int firstValue, int secondValue) {
        System.out.println("Operation result: " + (firstValue + secondValue));
    }

    /**
     * Метод подсчёта разности двух чисел
     *
     * @param firstValue  - первое значение
     * @param secondValue - второе значение
     * @return - результат
     */
    private void calculateDifference(int firstValue, int secondValue) {
        System.out.println("Operation result: " + (firstValue - secondValue));
    }

    /**
     * Метод подсчёта отношения двух чисел
     *
     * @param firstValue  - первое значение
     * @param secondValue - второе значение
     * @return - результат
     */
    private void calculateQuotient(int firstValue, int secondValue) {
        System.out.println("Operation result: " + ((double) firstValue / (double) secondValue));
    }

    /**
     * Метод подсчёта частного двух чисел
     *
     * @param firstValue  - первое значение
     * @param secondValue - второе значение
     * @return - результат
     */
    private void calculateProduct(int firstValue, int secondValue) {
        System.out.println("Operation result: " + (firstValue * secondValue));
    }
}

