package com.epam.kirill_barykin.java.lesson1.task2.tools;


import java.util.Scanner;

/**
 * Author: Kirill Barykin
 * Task №2
 * Ввод значений в консоль
 */

public class ConsoleInput {
    ConsoleOutput consoleOutput = new ConsoleOutput();

//    /**
//     * Получение количества строк, которые пользователь хочет ввести
//     */
//
//    public int numberOfRows1(int subTask) {
//        consoleOutput.subTaskNumber(subTask);
//        Scanner scanner = new Scanner(System.in);
//        System.out.print("Количество строк: ");
//        int numberOfRows = scanner.nextInt();
//
//        return numberOfRows;
//    }

    /**
     * Получение количества строк, которые пользователь хочет ввести
     */

    public int numberOfRows(int subTask) {
        consoleOutput.subTaskNumber(subTask);
        Scanner scanner = new Scanner(System.in);

        int numberOfRows;
        System.out.println();

        do {
            System.out.print("Enter the number of rows: ");
            while (!scanner.hasNextInt()) {
                System.out.print("It is not a number...\nEnter the number of rows: ");
                scanner.next();
            }
            numberOfRows = scanner.nextInt();
            if (numberOfRows <= 0)
                System.out.println("The number must be greater than zero");
        } while (numberOfRows <= 0);

        return numberOfRows;
    }

    /**
     * Получение строки, введённой пользователем
     */

    public String usersRow(int numberOfRow) {
        Scanner scanner = new Scanner(System.in);


        System.out.print("Enter the row №" + (numberOfRow) + ": ");
        String usersRow = scanner.nextLine();

        return usersRow;
    }

}
