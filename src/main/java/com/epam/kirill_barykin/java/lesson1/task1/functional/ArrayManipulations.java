package com.epam.kirill_barykin.java.lesson1.task1.functional;

import java.util.Arrays;

/**
 * Author: Kirill Barykin
 * Task №1
 * Манипуляции с массивом
 */

public class ArrayManipulations {

    /**
     * Генерация первоначального массива
     */

    public int[] generate() {
        System.out.print("Source array: ");
        int[] coreArray = new int[20];

        for (int i = 0; i < coreArray.length; i++) {
            coreArray[i] = (int) (Math.random() * 20 - 10);
        }

        System.out.println(Arrays.toString(coreArray));
        System.out.println();

        return coreArray;
    }

    /**
     * Сортировка слиянием (по возрастанию)
     *
     * @param coreArray - исходный массив
     * @return int[] с видоизменённым массивом
     */

    private int[] sortMerge(int[] coreArray) {

        int length = coreArray.length;
        if (length < 2) return coreArray;
        int middle = length / 2;
        return merge(sortMerge(Arrays.copyOfRange(coreArray, 0, middle)),
                sortMerge(Arrays.copyOfRange(coreArray, middle, length)));
    }

    private static int[] merge(int[] arrayA, int[] arrayB) {

        int lengthA = arrayA.length, lengthB = arrayB.length;

        int a = 0, b = 0, len = lengthA + lengthB;
        int[] result = new int[len];

        for (int i = 0; i < len; i++) {
            if (b < lengthB && a < lengthA) {
                if (arrayA[a] > arrayB[b]) result[i] = arrayB[b++];
                else result[i] = arrayA[a++];
            } else if (b < lengthB) {
                result[i] = arrayB[b++];
            } else {
                result[i] = arrayA[a++];
            }
        }
        return result;
    }

    /**
     * Получение массива положительных чисел в исходном массиве (и сортировка по возрастанию)
     *
     * @return
     */

    public int[] positiveArray(int[] coreArray) {

        int size = 0;
        int index = 0;

        for (int item : coreArray) {
            if (item >= 0)
                size++;
        }

        int[] positive = new int[size];
        for (int i = 0; i < coreArray.length; i++) {
            if (coreArray[i] >= 0) {
                positive[index] = coreArray[i];
                index++;
            }
        }
        return sortMerge(positive);

    }

    /**
     * Получение массива отрицательных чисел из исходного массива (и сортировка по возрастанию)
     */

    public int[] negativeArray(int[] coreArray) {

        int size = 0;
        int index = 0;

        for (int item : coreArray) {
            if (item < 0)
                size++;
        }

        int[] positive = new int[size];
        for (int i = 0; i < coreArray.length; i++) {
            if (coreArray[i] < 0) {
                positive[index] = coreArray[i];
                index++;
            }
        }
        return sortMerge(positive);
    }

    /**
     * Сумма чётных чисел
     *
     * @param coreArray - исходный массив
     * @return int с суммой чётных чисел
     */

    public int evenNumbersSum(int[] coreArray) {

        int evenSum = 0;

        for (int i = 0; i < coreArray.length; i = i + 2) {
            evenSum = evenSum + coreArray[i];
        }
        return evenSum;

    }

    /**
     * Заменить отрицательные числа на "0"
     *
     * @param coreArray - исходный массив
     * @return видоизменённый массив
     */

    public int[] replaceNegativeValuesByZeros(int[] coreArray) {
        int[] replacedArray = new int[coreArray.length];

        for (int i = 0; i < coreArray.length; i++) {
            if (coreArray[i] > 0) {
                replacedArray[i] = coreArray[i];
            }

        }

        return replacedArray;
    }

    /**
     * Утроить каждый положительный элемент, который стоит перед отрицательным
     *
     * @param coreArray - исходный массив
     * @return видоизменённый массив
     */

    public int[] triplePositiveValuesBeforeNegative(int[] coreArray) {

        int[] replacedArray = new int[coreArray.length];
        replacedArray[replacedArray.length - 1] = coreArray[coreArray.length - 1];

        for (int i = 0; i < coreArray.length - 1; i++) {
            if (coreArray[i] >= 0 && coreArray[i + 1] < 0) {
                replacedArray[i] = coreArray[i] * 3;
            } else {
                replacedArray[i] = coreArray[i];
            }
        }
        return replacedArray;
    }

    /**
     * Нахождение среднего арифметического
     *
     * @param coreArray - исходный массив
     * @return double со средним арифметическим
     */

    private double averageOfArray(int[] coreArray) {

        double sum = 0;
        double length = coreArray.length;

        for (int i = 0; i < coreArray.length; i++) {
            sum = sum + coreArray[i];
        }


        return sum / length;
    }

    /**
     * Разница между средним арифметичесикм и значением минимального элемента
     *
     * @param coreArray - исходный массив
     * @return int с Разница между средним арифметичесикм и значением минимального элемента
     */

    public double differenceBetweenAverageAndMinimalValue(int[] coreArray) {

        int minimal = sortMerge(coreArray)[0];

        return averageOfArray(coreArray) - minimal;
    }


    /**
     * Разница между средним арифметичесикм и значением минимального элемента (оптимизированный метод)
     *
     * @param coreArray
     * @return
     */
    public double differenceBetweenAverageAndMinimalValueEasy(int[] coreArray) {
        int minimal = 0;
        double sum = 0;
        double length = coreArray.length;

        for (int i = 0; i < coreArray.length; i++) {
            sum = sum + coreArray[i];
            if (coreArray[i] < coreArray[minimal]) {
                minimal = i;
            }
        }
        return (sum / length) - minimal;
    }

}
