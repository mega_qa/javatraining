package com.epam.kirill_barykin.java.lesson1.task2;

import com.epam.kirill_barykin.java.lesson1.task2.functional.RowManipulations;

/**
 * Author: Kirill Barykin
 * Task №2
 * Написать программу, осуществляющую обработку строк.
 * Варианты заданий:
 * 1. Ввести n строк с консоли, найти самую короткую и самую длинную
 * строки. Вывести найденные строки и их длину.
 * 2. Ввести n строк с консоли. Вывести на консоль те строки, длина
 * которых больше средней, а также длину.
 * 3. Ввести n строк с консоли. Вывести на консоль те строки, длина
 * которых меньше средней, а также длину.
 * 4. Ввести n слов с консоли. Найти слово, в котором число различных
 * символов минимально. Если таких слов несколько, найти первое из
 * них.
 * 5. Ввести n слов с консоли. Найти слово, состоящее только из
 * различных символов. Если таких слов несколько, найти первое из
 * них.
 * 6. Ввести n слов с консоли. Найти слово, состоящее только из цифр.
 * Если таких слов больше одного, найти второе из них.
 *
 */
public class RowProcessingApplication {
    public static void main(String[] args) {
        RowProcessingApplication rowProcessingApplication = new RowProcessingApplication();

        rowProcessingApplication.startApplication();
    }

    private void startApplication (){
        RowManipulations rowManipulations = new RowManipulations();

        rowManipulations.shortestAndLongestRowSearch(1);
        rowManipulations.longerThanAverage(2);
        rowManipulations.shorterThanAverage(3);
        rowManipulations.minimumNumberOfCharacters(4);
        rowManipulations.wordOfUniqueChars(5);
        rowManipulations.onlyNumbersInWord(6);
    }
}
