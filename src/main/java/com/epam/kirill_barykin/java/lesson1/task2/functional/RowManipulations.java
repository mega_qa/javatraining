package com.epam.kirill_barykin.java.lesson1.task2.functional;

import com.epam.kirill_barykin.java.lesson1.task2.tools.ConsoleInput;
import com.epam.kirill_barykin.java.lesson1.task2.tools.ConsoleOutput;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Author: Kirill Barykin
 * Task №2
 */

public class RowManipulations {

    /**
     * (1). Найти самую короткую и самую длинную строки. Вывести найденные строки и их длину.
     *
     * @param subTask - номер задания
     */
    public void shortestAndLongestRowSearch(int subTask) {
        ConsoleInput consoleInput = new ConsoleInput();

        int numberOfRows = consoleInput.numberOfRows(subTask);
        String[] usersRowsArray = new String[numberOfRows];

        for (int i = 0; i < numberOfRows; i++) {
            usersRowsArray[i] = consoleInput.usersRow(i + 1);
        }

        int longestRow = 0;
        int shortestRow = 0;

        for (int i = 0; i < numberOfRows; i++) {
            if (usersRowsArray[i].length() <= usersRowsArray[shortestRow].length())
                shortestRow = i;
            if (usersRowsArray[i].length() >= usersRowsArray[longestRow].length())
                longestRow = i;
        }

        System.out.println("Row with a minimum number of characters (" + usersRowsArray[shortestRow].length() + "): " + usersRowsArray[shortestRow]);
        System.out.println("Row with a maximum number of characters (" + usersRowsArray[longestRow].length() + "): " + usersRowsArray[longestRow]);
    }

    /**
     * (2). Отображение строк, которые длиннее, чем средняя введённая строка
     *
     * @param subTask - номер задания
     */
    public void longerThanAverage(int subTask) {
        ConsoleInput consoleInput = new ConsoleInput();

        int numberOfRows = consoleInput.numberOfRows(subTask);
        int average = 0;
        String[] usersRowsArray = new String[numberOfRows];

        for (int i = 0; i < numberOfRows; i++) {
            usersRowsArray[i] = consoleInput.usersRow(i + 1);
            average += usersRowsArray[i].length();
        }

        average /= numberOfRows;

        System.out.println("Average line length: " + average);


        int averageIndex = 0;
        for (int i = 0; i < numberOfRows; i++) {
            if (usersRowsArray[i].length() > average) {
                System.out.println("Row that is longer than average (Length: " + usersRowsArray[i].length() + "): \"" + usersRowsArray[i] + "\"");
                averageIndex++;
            }
        }
        if (averageIndex == 0) {
            System.out.println("Unable to find row longer than average length");
        }
    }


    /**
     * (3). Вывести на консоль те строки, длина которых меньше средней, а также длину.
     *
     * @param subTask - номер задания
     */
    public void shorterThanAverage(int subTask) {
        ConsoleInput consoleInput = new ConsoleInput();

        int numberOfRows = consoleInput.numberOfRows(subTask);
        int average = 0;
        String[] usersRowsArray = new String[numberOfRows];

        for (int i = 0; i < numberOfRows; i++) {
            usersRowsArray[i] = consoleInput.usersRow(i + 1);
            average += usersRowsArray[i].length();
        }

        average /= numberOfRows;

        System.out.println("Average line length: " + average);


        int averageIndex = 0;
        for (int i = 0; i < numberOfRows; i++) {
            if (usersRowsArray[i].length() < average) {
                System.out.println("Row that is longer than average (Length: " + usersRowsArray[i].length() + "): \"" + usersRowsArray[i] + "\"");
                averageIndex++;
            }
        }
        if (averageIndex == 0) {
            System.out.println("Unable to find row longer than average length");
        }
    }


    /**
     * (4). Ввести n слов с консоли. Найти слово, в котором число различных
     * символов минимально. Если таких слов несколько, найти первое из них.
     *
     * @param subTask
     */
    public void minimumNumberOfCharacters(int subTask) {
        Scanner scanner = new Scanner(System.in);
        ConsoleOutput consoleOutput = new ConsoleOutput();

        int numberOfWords;

        consoleOutput.subTaskNumber(subTask);
        System.out.print("Enter the number of words: ");
        if (scanner.hasNextInt()) {
            numberOfWords = Integer.parseInt(scanner.nextLine());

            if (numberOfWords > 0) {
                String[] usersWord = new String[numberOfWords];

                for (int i = 0; i < numberOfWords; i++) {
                    System.out.print("Enter the word №" + (i + 1) + ": ");
                    usersWord[i] = scanner.next();
                }

                String wordWithMinimumChars = usersWord[0];
                int numberOfCharsInWord = new HashSet<String>(Arrays.asList(usersWord[0].split(""))).size();

                for (String word : usersWord) {
                    HashSet<String> hashSet = new HashSet<String>(Arrays.asList(word.split("")));
                    if (hashSet.size() < numberOfCharsInWord) {
                        wordWithMinimumChars = word;
                        numberOfCharsInWord = hashSet.size();
                    }
                }
                System.out.println("A word with a minimum of various characters: " + wordWithMinimumChars);
            } else {
                System.out.println("You must enter a positive number. Try again.");
                minimumNumberOfCharacters(subTask);
            }
        } else {
            System.out.println("It is not a number. Try again.");
            minimumNumberOfCharacters(subTask);
        }
    }


    /**
     * (5). Ввести n слов с консоли. Найти слово, состоящее только из различных
     * символов. Если таких слов несколько, найти первое из них.
     */
    public void wordOfUniqueChars(int subTask) {
        ConsoleOutput consoleOutput = new ConsoleOutput();
        Scanner scanner = new Scanner(System.in);

        consoleOutput.subTaskNumber(subTask);
        System.out.print("Enter the number of words: ");
        int numberOfWords = Integer.parseInt(scanner.nextLine());

        int index = 0;
        String usersWord;
        String wordOfUniqueChars = null;

        for (int i = 0; i < numberOfWords; i++) {
            System.out.print("Enter the word №" + (i + 1) + ": ");
            usersWord = scanner.next();
            String[] chars = usersWord.split("");

            HashSet<String> uniqueChars = new HashSet<String>(Arrays.asList(chars));

            if (uniqueChars.size() == chars.length && index == 0) {
                wordOfUniqueChars = usersWord;
                index++;
            } else if (uniqueChars.size() != chars.length) {

            }

        }
        if (index == 0) {
            System.out.println("Words consisting of only different characters are missing");
        } else
            System.out.println("A word consisting only of different characters: " + wordOfUniqueChars);

    }


    /**
     * (6). Ввести n слов с консоли. Найти слово, состоящее только из цифр. Если
     * таких слов больше одного, найти второе из них.
     *
     * @param subTask - номер задания
     */
    public void onlyNumbersInWord(int subTask) {
        ConsoleOutput consoleOutput = new ConsoleOutput();

        Scanner scanner = new Scanner(System.in);

        consoleOutput.subTaskNumber(subTask);
        System.out.print("Enter the number of words: ");
        int numberOfWords = Integer.parseInt(scanner.nextLine());

        int index = 0;
        String usersWord;
        String numberWord = null;

        for (int i = 0; i < numberOfWords; i++) {
            System.out.print("Enter the word №" + (i + 1) + ": ");
            usersWord = scanner.nextLine();

            Pattern justNumbersPattern = Pattern.compile("^\\d++$"); // "^[-]?\d++$" - для поиска и отрицательных значений, но они являются не цифрами, а числами (что не соответствует условиям задачи).
            Matcher justNumber = justNumbersPattern.matcher(usersWord);

            if (justNumber.matches() && index < 2) {
                index++;
                numberWord = usersWord;
            }

        }

        if (index != 0)
            System.out.println("A word consisting only of numbers: " + numberWord);
        else
            System.out.println("There are no words consisting of numbers only");

    }
}
