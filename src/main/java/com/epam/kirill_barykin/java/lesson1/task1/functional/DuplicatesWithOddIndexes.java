package com.epam.kirill_barykin.java.lesson1.task1.functional;


import java.util.HashSet;

/**
 * Author: Kirill Barykin
 * Task №1
 * Поиск дубликатов с нечётными индексами
 */

public class DuplicatesWithOddIndexes {

    /**
     * Найти все элементы, которые встречаются больше одного раза и индексы которых нечётные.
     * Многомерный массив:
     * 0 - Значение из основного массива; 1 - признак наличия дубликата (0 = false, 1 = true)
     */

    public int[] duplicatesWithOddIndexes(int[] coreArray) {

        int[][] coreArrayMatrix = new int[2][coreArray.length];

        for (int i = 0; i < coreArray.length; i++) {
            coreArrayMatrix[0][i] = coreArray[i];
        }

        int index = 0; //размер нового массива
        for (int i = 0; i < coreArray.length; i++) {
            for (int j = 0; j < coreArray.length; j++) {
                if ((coreArray[i] == coreArray[j]) && (i != j)) {
                    coreArrayMatrix[1][i] = 1;
                }
            }
            if ((i % 2 == 0) && (coreArrayMatrix[1][i] == 1)) {
                index++;
            }
        }

        int[] duplicatesWithOddIndexes = new int[index];
        int oddIndex = 0;

        for (int i = 0; i < coreArray.length; i++) {
            if ((i % 2 != 0) && (coreArrayMatrix[1][i] == 1)) {
                duplicatesWithOddIndexes[oddIndex] = coreArray[i];
                oddIndex++;
            }
        }
        return duplicatesWithOddIndexes;
    }

//    public int[] duplicatesWithOddIndexesHashSet(int[] coreArray) {
//
//
//
//
//// Есть косяк с тем, что HashSet принимает только уникальные значения, из-за чего я периодически вываливаюсь за пределы массива.
//        HashSet <Integer> hashSet = new HashSet<Integer>();
//        for (int i = 0; i < coreArray.length; i++) {
//            for (int j = 0; j < coreArray.length; j++) {
//                if ((coreArray[i] == coreArray[j]) && (i != j)) {
//                    hashSet.add(coreArray[i]);
//                }
//            }
//        }
//
//        int[] duplicatesWithOddIndexes = new int[hashSet.size()+1];
//        int oddIndex = 0;
//
//        for (int i = 0; i < coreArray.length; i++) {
//            if ((i % 2 != 0) && (hashSet.contains(coreArray[i]))) {
//                duplicatesWithOddIndexes[oddIndex] = coreArray[i];
//                oddIndex++;
//            }
//        }
//        return duplicatesWithOddIndexes;
//    }

}
