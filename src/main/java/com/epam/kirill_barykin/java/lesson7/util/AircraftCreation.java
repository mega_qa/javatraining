package com.epam.kirill_barykin.java.lesson7.util;

import com.epam.kirill_barykin.java.lesson2.model.Aircraft;
import com.epam.kirill_barykin.java.lesson2.model.airplanes.A380;
import com.epam.kirill_barykin.java.lesson2.model.airplanes.Falcon;
import com.epam.kirill_barykin.java.lesson2.model.drones.MavicAir;
import com.epam.kirill_barykin.java.lesson2.model.helicopters.AtlasOrix;
import com.epam.kirill_barykin.java.lesson2.model.helicopters.Mi54;

import java.util.ArrayList;
import java.util.List;

public class AircraftCreation {

    public static List<Aircraft> aircraftList() {
        List<Aircraft> craft = new ArrayList<>();

        craft.add(new A380());
        craft.add(new Falcon());
        craft.add(new MavicAir());
        craft.add(new AtlasOrix());

        Mi54 mi54 = new Mi54(0, 1);
        mi54.setModel(null);
        mi54.setType(null);

        craft.add(mi54);

        return craft;
    }
}
