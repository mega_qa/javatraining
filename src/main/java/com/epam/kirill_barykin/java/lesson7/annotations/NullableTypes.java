package com.epam.kirill_barykin.java.lesson7.annotations;

import com.epam.kirill_barykin.java.lesson2.enums.AircraftType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface NullableTypes {
    AircraftType[] value();

    String message() default "Must return filled value";
}
