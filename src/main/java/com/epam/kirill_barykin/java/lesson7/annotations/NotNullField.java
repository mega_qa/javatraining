package com.epam.kirill_barykin.java.lesson7.annotations;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface NotNullField {
    String message() default "Field must be filled";
}
