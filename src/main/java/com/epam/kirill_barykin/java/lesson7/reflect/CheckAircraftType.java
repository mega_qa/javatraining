package com.epam.kirill_barykin.java.lesson7.reflect;

import com.epam.kirill_barykin.java.lesson2.model.Aircraft;
import com.epam.kirill_barykin.java.lesson7.util.ValidationProcessor;

import java.util.List;

import static com.epam.kirill_barykin.java.lesson7.util.AircraftCreation.aircraftList;

/**
 * Author: Kirill Barykin
 *
 * Создайте несколько аннотаций для вашей иерархии классов из 2 ДЗ. Например,
 * аннотации, проверяющие диапазоны значений типов, ненулевые значения, запрет
 * на использование каких-либо значений и т.д по желанию ментора.
 * Напишите свой процессор аннотаций, основанный на Reflection API, который парсит
 * классы и «применяет» аннотации к экземплярам классов из иерархии (2 ДЗ).
 * Предоставьте тестовый код, где инициализируются экземпляры классов из ирерахии (2 ДЗ)
 * и запускается процессор аннтоаций, выдающий warnings на консоль, если поле
 * проинициализировано запрещенным значением, например (и вообще во всех
 * случаях, когда содержимое private-полей нарушает ограничения заданные в
 * аннотациях).
 *
 */
public class CheckAircraftType {

    public static void main(String[] args) {
        CheckAircraftType checkAircraftType = new CheckAircraftType();

        checkAircraftType.startApplication();
    }

    public void startApplication() {
        List<Aircraft> craft = aircraftList();

        ValidationProcessor.validation(craft);
    }


}
