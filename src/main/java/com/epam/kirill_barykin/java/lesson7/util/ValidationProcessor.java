package com.epam.kirill_barykin.java.lesson7.util;

import com.epam.kirill_barykin.java.lesson2.enums.AircraftType;
import com.epam.kirill_barykin.java.lesson7.annotations.MinDistance;
import com.epam.kirill_barykin.java.lesson7.annotations.NotNullField;
import com.epam.kirill_barykin.java.lesson7.annotations.NullableTypes;
import com.epam.kirill_barykin.java.lesson7.annotations.Range;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ValidationProcessor {
    private static Logger logger = Logger.getLogger(ValidationProcessor.class.getName());

    /**
     * Field and method validation
     *
     * @param target
     */
    public static void validation(List<?> target) {
        target.forEach(it -> {
            Class superClass = it.getClass().getSuperclass();
            Arrays.stream(superClass.getDeclaredFields()).forEach(superClassField -> {
                Arrays.stream(superClassField.getAnnotations()).forEach(annotation -> {
                    if (annotation != null) {

                        if (annotation instanceof MinDistance) {
                            MinDistance minDistance = (MinDistance) annotation;
                            try {
                                superClassField.setAccessible(true);
                                int val = superClassField.getInt(it);
                                if (val < minDistance.value()) {
                                    logger.log(Level.WARNING, minDistance.message() + " (" + "ClassName: " + it.getClass().getSimpleName() + ", FieldName: " + superClassField.getName() + ")");
                                }
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        if (annotation instanceof NotNullField) {
                            NotNullField notNullField = (NotNullField) annotation;
                            try {
                                superClassField.setAccessible(true);
                                if (superClassField.get(it) == null) {
                                    logger.log(Level.WARNING, notNullField.message() + " (" + "ClassName: " + it.getClass().getSimpleName() + ", FieldName: " + superClassField.getName() + ")");
                                }
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        if (annotation instanceof Range) {
                            Range range = (Range) annotation;
                            try {
                                superClassField.setAccessible(true);
                                int fieldValue = superClassField.getInt(it);
                                if (fieldValue < range.min() || fieldValue > range.max()) {
                                    logger.log(Level.WARNING, range.message() + " (" + "ClassName: " + it.getClass().getSimpleName() + ", FieldName: " + superClassField.getName() + ")");
                                }
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            });
        });

        target.forEach(it -> {
            Class superClass = it.getClass().getSuperclass();
            Arrays.stream(superClass.getMethods()).forEach(superClassMethod -> {
                Arrays.stream(superClassMethod.getAnnotations()).forEach(annotation -> {
                    if (annotation != null) {
                        if (annotation instanceof NullableTypes) {
                            NullableTypes notNullifAircraftTypeIsNot = (NullableTypes) annotation;
                            try {
                                superClassMethod.setAccessible(true);
                                Field aircraftTypeField = superClass.getDeclaredField("type");
                                aircraftTypeField.setAccessible(true);
                                AircraftType aircraftType = (AircraftType) aircraftTypeField.get(it);
                                boolean fits = false;
                                for (int i = 0; i < notNullifAircraftTypeIsNot.value().length; i++) {
                                    if (notNullifAircraftTypeIsNot.value()[i] == aircraftType) {
                                        fits = true;
                                    }
                                }
                                if (superClassMethod.invoke(it).equals(0) && !fits) {
                                    logger.log(Level.WARNING, notNullifAircraftTypeIsNot.message() + " (" + "ClassName: " + it.getClass().getSimpleName() + ", MethodName: " + superClassMethod.getName() + ")");
                                }
                            } catch (IllegalAccessException | InvocationTargetException | NoSuchFieldException e) {
                                e.printStackTrace();
                            }
                        }

                        if (annotation instanceof Range) {
                            Range range = (Range) annotation;
                            try {
                                superClassMethod.setAccessible(true);
                                int methodResult = Integer.parseInt(superClassMethod.invoke(it).toString());
                                if (methodResult < range.min() || methodResult > range.max()) {
                                    logger.log(Level.WARNING, range.message() + " (" + "ClassName: " + it.getClass().getSimpleName() + ", MethodName: " + superClassMethod.getName() + ")");
                                }
                            } catch (IllegalAccessException | InvocationTargetException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            });
        });
    }
}
