package com.epam.kirill_barykin.java.lesson3.exeptions;

public final class EmptyArrayException extends Exception {

    public EmptyArrayException(String message){
        super(message);
    }
}
