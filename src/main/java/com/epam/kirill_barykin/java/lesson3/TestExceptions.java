package com.epam.kirill_barykin.java.lesson3;

import com.epam.kirill_barykin.java.lesson2.model.Aircraft;
import com.epam.kirill_barykin.java.lesson2.model.AircraftFilter;
import com.epam.kirill_barykin.java.lesson2.model.Airline;
import com.epam.kirill_barykin.java.lesson2.model.airplanes.A380;
import com.epam.kirill_barykin.java.lesson2.model.airplanes.Falcon;
import com.epam.kirill_barykin.java.lesson2.model.drones.MavicAir;
import com.epam.kirill_barykin.java.lesson2.model.helicopters.AtlasOrix;
import com.epam.kirill_barykin.java.lesson2.model.helicopters.Mi54;
import com.epam.kirill_barykin.java.lesson3.exeptions.EmptyArrayException;
import com.epam.kirill_barykin.java.lesson3.exeptions.MaximumLoadException;
import com.epam.kirill_barykin.java.lesson3.exeptions.MaximumNumberOfPassengersException;
import com.epam.kirill_barykin.java.lesson3.exeptions.RangeOfFlightException;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Author: Kirill Barykin
 *
 * Task 3:
 * "Спроектировать иерархию исключительных ситуаций для объектной
 * модели, выбранной в предыдущем домашнем задании."
 *
 */

public final class TestExceptions {
    private static final Logger logger = Logger.getLogger(TestExceptions.class.getName());
    Airline airline = new Airline(new Aircraft[]{
            new Falcon(),
            new MavicAir(),
            new Mi54(),
            new A380(),
            new AtlasOrix()
    });

    public static void main(String[] args) {
        TestExceptions testExceptions = new TestExceptions();

        testExceptions.catchMaximumLoadException();
        testExceptions.catchMaximumNumberOfPassengersException();
        testExceptions.catchRangeOfFlightException();
        testExceptions.multicatchFilterException();
        testExceptions.catchEmptyArrayException();
    }

    /**
     * Catch MaximumLoadException
     *
     */
    private void catchMaximumLoadException(){
        AircraftFilter aircraftFilter = new AircraftFilter();
        aircraftFilter.setMaximumLoad(-1);

        try{
            airline.getAircraft(aircraftFilter);
        }catch (MaximumLoadException e){
            logger.log(Level.WARNING, e.getMessage(), e);
        }
    }

    /**
     * Catch MaximumNumberOfPassengersException
     *
     */
    private void catchMaximumNumberOfPassengersException(){
        AircraftFilter aircraftFilter = new AircraftFilter();
        aircraftFilter.setMaximumNumberOfPassengers(-1);

        try{
            airline.getAircraft(aircraftFilter);
        }catch (MaximumNumberOfPassengersException e){
            logger.log(Level.WARNING, e.getMessage(), e);
        }
    }

    /**
     * Catch RangeOfFlightException
     *
     */
    private void catchRangeOfFlightException(){
        AircraftFilter aircraftFilter = new AircraftFilter();
        aircraftFilter.setRangeOfFlight(-1);

        try{
            airline.getAircraft(aircraftFilter);
        }catch (RangeOfFlightException e){
            logger.log(Level.WARNING, e.getMessage(), e);
        }
    }

    /**
     * Catch multiple exceptions (MaximumLoadException, MaximumNumberOfPassengersException, RangeOfFlightException)
     * and using "finally" module
     *
     */
    private void multicatchFilterException(){
        AircraftFilter aircraftFilter = new AircraftFilter();
        boolean errorExist = false;

        aircraftFilter.setRangeOfFlight((int) (Math.random() * 20 - 10));
        aircraftFilter.setMaximumLoad((int) (Math.random() * 20 - 10));
        aircraftFilter.setMaximumNumberOfPassengers((int) (Math.random() * 20 - 10));
        try{
            airline.getAircraft(aircraftFilter);
        }catch (MaximumLoadException | MaximumNumberOfPassengersException | RangeOfFlightException e){
            logger.log(Level.WARNING, "There is some error: " + e.getMessage(), e);
            errorExist = true;
        }finally {
            if (!errorExist){
                System.out.println("There is no errors");
            }
        }

    }

    /**
     * Catch EmptyArrayException
     *
     */
    private void catchEmptyArrayException(){
        Aircraft[] airline = new Aircraft[0];
        try{
            this.airline.sortMerge(airline);
        }catch (EmptyArrayException e){
            logger.log(Level.WARNING,"Failed get aircraft!", e);
        }
    }


}
