package com.epam.kirill_barykin.java.lesson3.exeptions;

public final class MaximumNumberOfPassengersException extends AircraftFilterValidityException {

    public MaximumNumberOfPassengersException(){
        super();
    }
}
