package com.epam.kirill_barykin.java.lesson3.exeptions;

public class AircraftFilterValidityException extends RuntimeException {

    private AircraftFilterValidityException(String message){
        super(message);
    }

    public AircraftFilterValidityException(){
        this("Passed incorrect value");
    }
}

