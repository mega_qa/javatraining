package com.epam.kirill_barykin.java.lesson4.task2.streamAPI;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StreamCollectionManipulations {

    private List<Integer> coreList;
    private List<Integer> shuffledList;

    public static void main(String[] args) {
        StreamCollectionManipulations streamCollectionManipulations = new StreamCollectionManipulations();
        streamCollectionManipulations.startApplication();
    }

    public void startApplication() {
        StreamCollectionManipulations streamCollectionManipulations = new StreamCollectionManipulations();

        streamCollectionManipulations.generateSomeNumbers();
        streamCollectionManipulations.randomOrder();
        streamCollectionManipulations.showThatOrderIsRandom();
        streamCollectionManipulations.isNumbersUnique();
        streamCollectionManipulations.minimalValue();
        streamCollectionManipulations.removeOddValues();
        streamCollectionManipulations.penultimateElement();
    }


    /**
     * Generate 1 000 000 numbers in ascending order
     */
    private void generateSomeNumbers() {
        coreList = IntStream
                .rangeClosed(1, 1000000)
                .boxed()
                .collect(Collectors.toList());
    }

    /**
     * Shuffle coreList to make the order random
     * @return
     */
    private List<Integer> randomOrder() {
        shuffledList = new ArrayList<>(coreList);
        Collections.shuffle(shuffledList);
        return shuffledList;
    }

    /**
     * Show that the order is really random
     */
    public void showThatOrderIsRandom() {
        System.out.print("First 15 numbers in random order: ");

        randomOrder().stream()
                .limit(15)
                .forEach(e -> System.out.print(e + " "));
        System.out.println();
    }

    /**
     * Stacks all the elements in HashMap to get rid of non-unique elements
     */
    private List<Integer> uniqueNumbers() {
        return coreList.stream()
                .distinct()
                .collect(Collectors.toList());
    }

    /**
     * Show that numbers in coreList is unique
     */
    public void isNumbersUnique() {
        System.out.println("All numbers are unique: " + (coreList.size() == uniqueNumbers().size()));
    }

    /**
     * Sort coreList by ASC
     */
    private void sortList() {
        coreList = coreList.stream()
                .sorted()
                .collect(Collectors.toList());
    }

    /**
     * Find minimal value in coreList
     */
    public void minimalValue() {
        int minimalElement = coreList.stream()
                .min(Integer::compareTo)
                .orElseThrow(()-> new IllegalArgumentException("Target List is empty"));

        System.out.println("Minimum element: " + minimalElement);
    }

    /**
     * Remove odd values from coreList
     */
    public void removeOddValues() {
        coreList.stream()
                .filter(it -> it % 2 == 0)
                .collect(Collectors.toList());
    }

    /**
     * Find penultimate element in coreList
     */
    public void penultimateElement() {
        sortList();
        int penultimateNumber = coreList.stream()
                .skip(coreList.size() - 2)
                .findFirst()
                .get();
        System.out.println("Penultimate element: " + penultimateNumber);
    }

}