package com.epam.kirill_barykin.java.lesson4.task2;

import java.util.*;

/**
 * Author: Kirill Barykin
 *
 * Необходимо проделать определенные трансформации с данными,
 * используя для это корректные коллекции и алгоритмы из Java Collection Framework:
 * I.Сгенерируйте 1 000 000 последовательных целых чисел
 * II.Создайте коллекцию A и вставьте туда элементы
 * последовательности в произвольном порядке
 * III.Покажите, что порядок произвольный
 * IV.Напишите проверку на то, что все элементы в данной
 * коллекции A уникальны (докажите это каким-либо
 * способом)
 * V.Найдите минимальный элемент в данной
 * последовательности
 * VI.Удалите все нечетные элементы из последовательности
 * VII.Найдите предпоследний по величине элемент
 * VIII.* Повторите все указанные действия, но при помощи
 * Stream API (бонусная задача)
 *
 */
public class CollectionManipulations {
    ArrayList <Integer> coreList;

    public static void main(String[] args) {
        CollectionManipulations collectionManipulations = new CollectionManipulations();

        collectionManipulations.startApplication();
    }

    public void startApplication(){
        CollectionManipulations collectionManipulations = new CollectionManipulations();

        collectionManipulations.randomOrder();
        collectionManipulations.showThatOrderIsRandom();
        collectionManipulations.isNumbersUnique();
        collectionManipulations.minimalValue();
        collectionManipulations.removeOddValues();
        collectionManipulations.penultimateElement();
    }


    /**
     * Generate 1 000 000 numbers in ascending order
     *
     * @return
     */
    private ArrayList <Integer> generateSomeNumbers(){
        ArrayList <Integer> generatedList = new ArrayList<>();
        for (int i = 1; i < 1000001; i++) {
            generatedList.add(i);
        }
        return generatedList;
    }

    /**
     * Shuffle coreList to make the order random
     *
     */
    public void randomOrder(){
        coreList = generateSomeNumbers();
        Collections.shuffle(coreList);
    }

    /**
     * Show that the order is really random
     *
     */
    public void showThatOrderIsRandom(){
        System.out.print("First 15 numbers in random order: ");
        for (int i = 0; i < 15; i++) {
            System.out.print(coreList.get(i) + " ");
        }
        System.out.println();
    }

    /**
     * Stacks all the elements in HashMap to get rid of non-unique elements
     *
     * @return
     */
    private Map<Integer, Boolean> uniqueNumbers(){
        Map<Integer, Boolean> uniqueNumbers = new HashMap<>();

        for (int i = 0; i < coreList.size(); i++) {
            uniqueNumbers.put(coreList.get(i), true);
        }

        return uniqueNumbers;
    }

    /**
     * Show that numbers in coreList is unique
     *
     */
    public void isNumbersUnique() {
        System.out.println("All numbers are unique: " + Boolean.toString(coreList.size() == uniqueNumbers().size()));
    }

    /**
     * Sort coreList by ASC
     *
     */
    private void sortList(){
        coreList.sort(new Comparator<Integer>() {
            public int compare(Integer a, Integer b) {
                return a - b;
            }
        });
    }

    /**
     * Find minimal value in coreList
     *
     */
    public void minimalValue(){
        sortList();
        System.out.println("Minimum element: " + coreList.get(0));
    }

    /**
     * Remove odd values from coreList
     *
     */
    public void removeOddValues(){
        ArrayList<Integer> listWithoutOddValues = new ArrayList<>();

        for (int i = 0; i < coreList.size(); i++) {
            if (coreList.get(i) % 2 == 0) {
                listWithoutOddValues.add(coreList.get(i));
            }
        }
    }

    /**
     * Find penultimate element in coreList
     *
     */
    public void penultimateElement(){
        sortList();
        System.out.println("Penultimate element: " + coreList.get(coreList.size() - 2));
    }

}
