package com.epam.kirill_barykin.java.lesson4.task1;

import java.util.Arrays;

/**
 * Author: Kirill Barykin
 *
 * Создать собственный класс DynamicArray, содержащий внутри себя
 * массив, поддерживающий добавление элемента в конец массива (add),
 * получение элемента по индексу (get), печать внутреннего состояния
 * (toString), удаление произвольного элемента по индексу (remove), а
 * также возможность задать начальный размер при вызове конструктора.
 * При изменении количества элементов внутренний массив должен
 * пересоздаваться.
 *
 * @param <T>
 */

public class DynamicArray<T> {

    private Object[] coreArray;
    private int currentSize;

    public DynamicArray() {
        coreArray = new Object[16];
    }

    /**
     * Set array length
     *
     * @param arrayLength - array length
     */
    public DynamicArray(int arrayLength) {
        coreArray = new Object[arrayLength];
    }

    /**
     * Adding an element to the end of an array
     *
     * @param additionalValue - value to be added
     */
    public void add(T additionalValue) {

        if (currentSize >= coreArray.length - 1) {
            Object[] extendedArray = coreArray;
            coreArray = new Object[currentSize * 2];
            coreArray = Arrays.copyOf(extendedArray, coreArray.length);
        }
        coreArray[currentSize] = additionalValue;
        currentSize++;
    }

    /**
     * Removing an element on desired position
     *
     * @param index - value to be added
     */
    public void remove(int index) {

        if (index <= currentSize - 1) {
            int reducedArrayPosition = 0;
            Object[] reducedArray = new Object[coreArray.length];

            for (int i = 0; i < currentSize; i++) {
                if (i != index) {
                    reducedArray[reducedArrayPosition] = coreArray[i];
                    reducedArrayPosition++;
                }
            }
            coreArray = reducedArray;
            currentSize--;
        }
    }

    /**
     * Getting value by index
     *
     * @param index - desired position
     * @return - value on desired position
     */
    public T get(int index) {
        if (index <= currentSize - 1) {
            if (coreArray[index] != null) {
                return (T) coreArray[index];
            } else {
                throw new IllegalArgumentException("Chosen value is empty");
            }
        } else {
            throw new IllegalArgumentException("Array length is smaller then chosen index");
        }
    }

    /**
     * Array output (normal view)
     */
    public String toString() {
        return Arrays.deepToString(coreArray);
    }


}

