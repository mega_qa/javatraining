package com.epam.kirill_barykin.java.lesson6;

import com.epam.kirill_barykin.java.lesson6.enums.DataType;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class TextManipulations {

    /**
     * Get list of strings in specified file
     *
     * @param dataType
     * @return
     */
    public static ArrayList<String> listData(DataType dataType) {
        String filePath = dataType.getPath();
        ArrayList<String> dataList = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath))) {
            String sCurrentLine;
            while ((sCurrentLine = bufferedReader.readLine()) != null) {
                dataList.add(sCurrentLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dataList;
    }
}
