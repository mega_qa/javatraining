package com.epam.kirill_barykin.java.lesson6;

import com.epam.kirill_barykin.java.lesson6.enums.IDType;

import java.sql.*;
import java.util.ArrayList;
import java.util.Random;

import static com.epam.kirill_barykin.java.lesson6.DataGeneration.randomElementFromList;

public class DataBaseManipulations {

//---------------------------------------------------------------------------------------------------------------------------------------------

    private static final String URL = "jdbc:mysql://localhost:3306/";
    private static final String DB_NAME = "vepamke";
    private static final String DB_TIME_ZONE_QUERY = "?useUnicode=true&rewriteBatchedStatements=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String USER_NAME = "root";
    private static final String PASSWORD = "Root1";

    private static final int USERS_MIN_RANGE = 1001;
    private static final int USERS_MAX_RANGE = 1200;
    private static final int FRIENDSHIPS_MIN_RANGE = 70000;
    private static final int FRIENDSHIPS_MAX_RANGE = 70200;
    private static final int POSTS_MIN_RANGE = 40000;
    private static final int POSTS_MAX_RANGE = 45000;
    private static final int LIKES_MIN_RANGE = 300000;
    private static final int LIKES_MAX_RANGE = 350000;


    private static final String CREATE_TABLE_USERS = "CREATE TABLE IF NOT EXISTS `vepamke`.`users` (" +
            "`id` INT NOT NULL AUTO_INCREMENT," +
            "`name` VARCHAR(90) NOT NULL," +
            "`surname` VARCHAR(90) NOT NULL," +
            "`birthdate` DATE NOT NULL," +
            "PRIMARY KEY (`id`))" +
            "DEFAULT CHARACTER SET = utf8;";
    private static final String CREATE_TABLE_FRIENDSHIPS = "CREATE TABLE IF NOT EXISTS `vepamke`.`friendships` (" +
            "  `userid1` INT NOT NULL," +
            "  `userid2` INT NOT NULL," +
            "  `timestamp` TIMESTAMP(6) NOT NULL)" +
            "DEFAULT CHARACTER SET = utf8;";
    private static final String CREATE_TABLE_POSTS = "CREATE TABLE IF NOT EXISTS `vepamke`.`posts` (" +
            "  `id` INT NOT NULL AUTO_INCREMENT," +
            "  `userid` INT NOT NULL," +
            "  `text` TEXT(140) NULL," +
            "  `timestamp` TIMESTAMP(6) NOT NULL," +
            "  PRIMARY KEY (`id`)," +
            "  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE)" +
            "DEFAULT CHARACTER SET = utf8;";
    private static final String CREATE_TABLE_LIKES = "CREATE TABLE IF NOT EXISTS `vepamke`.`likes` (" +
            "  `postid` INT NOT NULL," +
            "  `userid` INT NOT NULL," +
            "  `timestamp` TIMESTAMP(6) NOT NULL)" +
            "DEFAULT CHARACTER SET = utf8;";

    private static final String INSERT_NEW_USER = "INSERT INTO users (name, surname, birthdate) VALUES (?,?,?)";
    private static final String CREATE_NEW_FRIENDSHIP = "INSERT INTO friendships VALUES (?,?,?)";
    private static final String CREATE_NEW_POST = "INSERT INTO posts (userid, text, timestamp) VALUES (?,?,?)";
    private static final String LIKE_IT = "INSERT INTO likes VALUES (?,?,?)";

//---------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Create tables "users", "friendships", "posts" and "likes" in DB
     *
     * @throws SQLException
     */
    public void createTables(Connection connection) {

        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(CREATE_TABLE_USERS);
            statement.executeUpdate(CREATE_TABLE_FRIENDSHIPS);
            statement.executeUpdate(CREATE_TABLE_POSTS);
            statement.executeUpdate(CREATE_TABLE_LIKES);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Fill up table "users" with random data
     *
     * @param userNames
     * @param userSurnames
     */
    public void createRandomUser(ArrayList<String> userNames, ArrayList<String> userSurnames, Connection connection) {
        DataGeneration dataGeneration = new DataGeneration();

        try (PreparedStatement statement = connection.prepareStatement(INSERT_NEW_USER)) {
            int limit = dataGeneration.randomNumberInGivenRange(USERS_MIN_RANGE, USERS_MAX_RANGE);
            for (int i = 0; i < limit; i++) {
                Random random = new Random();

                // Get an Epoch value roughly between 1940 and 2010
                // -946771200000L = January 1, 1940
                // Add up to 70 years to it (using modulus on the next long)
                long millis = -946771200000L + (Math.abs(random.nextLong()) % (70L * 365 * 24 * 60 * 60 * 1000));

                statement.setString(1, randomElementFromList(userNames));
                statement.setString(2, randomElementFromList(userSurnames));
                statement.setDate(3, new Date(millis));

                statement.addBatch();

                if (i % 1000 == 0 || i == limit - 1) {
                    statement.executeBatch();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Make random users friends
     * (Fill up table "friendships" with random data)
     */
    public void makeThemFriends(ArrayList<Integer> userIds, Connection connection) {
        DataGeneration dataGeneration = new DataGeneration();

        int userid_1;
        int userid_2;

        try (PreparedStatement statement = connection.prepareStatement(CREATE_NEW_FRIENDSHIP)) {
            if (userIds != null) {
                int limit = dataGeneration.randomNumberInGivenRange(FRIENDSHIPS_MIN_RANGE, FRIENDSHIPS_MAX_RANGE);
                for (int i = 0; i < limit; i++) {
                    userid_1 = randomElementFromList(userIds);
                    userid_2 = randomElementFromList(userIds, userid_1);

                    statement.setInt(1, userid_1);
                    statement.setInt(2, userid_2);
                    statement.setTimestamp(3, getRandomTimestamp());

                    statement.addBatch();

                    if (i % 1000 == 0 || i == limit - 1) {
                        statement.executeBatch();
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create random post by random user
     * (Fill up table "posts" with random data)
     *
     * @param postTexts
     * @throws SQLException
     */
    public void makeSomePost(ArrayList<String> postTexts, ArrayList<Integer> userIds, Connection connection) {
        DataGeneration dataGeneration = new DataGeneration();

        try (PreparedStatement statement = connection.prepareStatement(CREATE_NEW_POST)) {
            int limit = dataGeneration.randomNumberInGivenRange(POSTS_MIN_RANGE, POSTS_MAX_RANGE);
            for (int i = 0; i < limit; i++) {
                statement.setInt(1, randomElementFromList(userIds));
                statement.setString(2, randomElementFromList(postTexts));
                statement.setTimestamp(3, getRandomTimestamp());

                statement.addBatch();

                if (i % 1000 == 0 || i == limit - 1) {
                    statement.executeBatch();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create random likes for random posts
     *
     * @param postIds
     * @param userIds
     * @param connection
     */
    public void likeIt(ArrayList<Integer> postIds, ArrayList<Integer> userIds, Connection connection) {
        DataGeneration dataGeneration = new DataGeneration();

        try (PreparedStatement statement = connection.prepareStatement(LIKE_IT)) {
            int limit = dataGeneration.randomNumberInGivenRange(LIKES_MIN_RANGE, LIKES_MAX_RANGE);

            for (int i = 0; i < limit; i++) {
                statement.setInt(1, randomElementFromList(postIds));
                statement.setInt(2, randomElementFromList(userIds));
                statement.setTimestamp(3, getRandomTimestamp());

                statement.addBatch();

                if (i % 1000 == 0 || i == limit - 1) {
                    statement.executeBatch();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

//---------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Get random timestamp between 01-01-2015 and 31-12-2015
     *
     * @return
     */
    private Timestamp getRandomTimestamp() {
        long offset = Timestamp.valueOf("2015-01-01 00:00:00").getTime();
        long end = Timestamp.valueOf("2015-12-31 00:00:00").getTime();
        long diff = end - offset + 1;
        return new Timestamp(offset + (long) (Math.random() * diff));
    }

    public static Connection getConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(URL + DB_NAME + DB_TIME_ZONE_QUERY, USER_NAME, PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public void closeConnection(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Get all IDs from table "users" or "posts"
     *
     * @param idType
     * @param connection
     * @return
     * @throws SQLException
     */
    public static ArrayList<Integer> getAllIds(IDType idType, Connection connection) {
        ArrayList<Integer> idList = new ArrayList<>();
        String dataTable = idType.getRequest();

        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(dataTable);
            while (resultSet.next()) {
                idList.add(resultSet.getInt("id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return idList;
    }
}
