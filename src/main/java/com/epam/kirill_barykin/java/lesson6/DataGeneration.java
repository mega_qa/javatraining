package com.epam.kirill_barykin.java.lesson6;

import com.epam.kirill_barykin.java.lesson6.enums.DataType;
import com.epam.kirill_barykin.java.lesson6.enums.IDType;

import java.sql.*;
import java.util.ArrayList;
import java.util.Random;

import static com.epam.kirill_barykin.java.lesson6.DataBaseManipulations.getAllIds;
import static com.epam.kirill_barykin.java.lesson6.DataBaseManipulations.getConnection;
import static com.epam.kirill_barykin.java.lesson6.TextManipulations.listData;

public class DataGeneration {

    /**
     * Fill up all tables with random data
     */
    public void createRandomRecords() {
        DataBaseManipulations dataBaseManipulations = new DataBaseManipulations();

        ArrayList<String> userNames = listData(DataType.NAME);
        ArrayList<String> userSurnames = listData(DataType.SURNAME);
        ArrayList<String> postTexts = listData(DataType.TEXT);

        Connection connection = getConnection();

        dataBaseManipulations.createTables(connection);
        dataBaseManipulations.createRandomUser(userNames, userSurnames, connection);
        ArrayList<Integer> userIds = getAllIds(IDType.USER, connection);
        dataBaseManipulations.makeThemFriends(userIds, connection);
        dataBaseManipulations.makeSomePost(postTexts, userIds, connection);

        ArrayList<Integer> postIds = getAllIds(IDType.POST, connection);
        dataBaseManipulations.likeIt(postIds, userIds, connection);

        dataBaseManipulations.closeConnection(connection);
    }

    /**
     * Get random number in given range
     *
     * @param min
     * @param max
     * @return
     */
    public int randomNumberInGivenRange(int min, int max) {
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }

    /**
     * Get random string from ArrayList
     *
     * @param stringList - ArrayList of Strings received from file
     * @return
     */
    public static <T> T randomElementFromList(ArrayList<T> stringList) {
        Random random = new Random();
        return stringList.get(random.nextInt(stringList.size()));
    }

    public static <T> T randomElementFromList(ArrayList<T> stringList, T value) {
        Random random = new Random();
        T randomElement;
        do {
            randomElement = stringList.get(random.nextInt(stringList.size()));
        } while (randomElement != value);
        return randomElement;
    }
}
