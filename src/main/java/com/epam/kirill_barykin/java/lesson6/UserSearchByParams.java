package com.epam.kirill_barykin.java.lesson6;

import static com.epam.kirill_barykin.java.lesson6.DataBaseManipulations.getConnection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class UserSearchByParams {
    public static final String SEARCH_USER_NAMES_BY_PARAMS =
            "SELECT DISTINCT u.name, avg_likes " +
                    "FROM users u  " +
                    "JOIN friendships f ON (f.userid1 = u.id OR f.userid2 = u.id) AND f.timestamp < '2015-03-31' " +
                    "JOIN ( " +
                    " SELECT AVG(like_count) avg_likes, pst.userid  " +
                    " FROM posts pst  " +
                    " JOIN ( " +
                    "  SELECT count(1)  " +
                    "        AS like_count, p.id  " +
                    "        FROM posts p  " +
                    "        JOIN likes lk  " +
                    "        ON lk.postId = p.id  " +
                    "        GROUP BY p.id " +
                    "        ) cnt " +
                    " ON cnt.id = pst.id  " +
                    "    GROUP BY pst.userid " +
                    "    ) pp  " +
                    "ON pp.userid = u.id  " +
                    "AND pp.avg_likes >= 3 AND pp.avg_likes < 15  " +
                    "GROUP BY u.id " +
                    "HAVING count(DISTINCT f.userid1, f.userid2) > 50;";

    /**
     * Print sorted users
     */
    public void printSortedUsers() {
        ArrayList<String> usersNames;
        String message = "There is no suitable users";
        try {
            usersNames = foundUsers();
            if (!usersNames.isEmpty()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Users with unique names with more then 100 friends in March 2015 and the average number of likes of each post (for the entire period) is between [3; 15):");
                sb.append("\n");
                sb.append(usersNames);
                message = sb.toString();
            }
            System.out.println(message);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * Search user's unique names with more then 100 friends in March 2015 and the average number of likes of each
     * post (for the entire period) is between [3; 15)
     *
     * @return
     * @throws SQLException
     */
    private ArrayList<String> foundUsers() throws SQLException {
        DataBaseManipulations dataBaseManipulations = new DataBaseManipulations();
        Connection connection = getConnection();
        Statement statement;
        ArrayList<String> usersNames = new ArrayList<>();

        statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(SEARCH_USER_NAMES_BY_PARAMS);
        while (resultSet.next()) {
            usersNames.add(resultSet.getString("name"));
        }
        dataBaseManipulations.closeConnection(connection);
        return usersNames;
    }
}
