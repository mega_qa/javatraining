package com.epam.kirill_barykin.java.lesson6.enums;

import java.io.File;
import java.util.StringJoiner;

public enum DataType {
    NAME {
        @Override
        public String getPath() {
            return specifyPath("names");
        }
    },
    SURNAME {
        @Override
        public String getPath() {
            return specifyPath("surnames");
        }
    },
    TEXT {
        @Override
        public String getPath() {
            return specifyPath("postTexts");
        }
    };

    public abstract String getPath();

    protected String specifyPath(String filename) {
        StringJoiner joiner = new StringJoiner(File.separator);
        joiner.add(System.getProperty("user.dir"))
                .add("src")
                .add("main")
                .add("resources")
                .add("lesson6")
                .add("textFiles")
                .add(filename + ".txt");

        return joiner.toString();
    }
}
