package com.epam.kirill_barykin.java.lesson6.enums;

public enum IDType {
    USER {
        @Override
        public String getRequest() {
            return "SELECT id FROM users";
        }
    },
    POST {
        @Override
        public String getRequest() {
            return "SELECT id FROM posts";
        }
    };

    public abstract String getRequest();
}
