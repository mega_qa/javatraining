package com.epam.kirill_barykin.java.lesson5.task1;

import com.epam.kirill_barykin.java.lesson2.model.BaseAircraft;
import com.epam.kirill_barykin.java.lesson2.model.airplanes.A380;
import com.epam.kirill_barykin.java.lesson2.model.airplanes.Falcon;
import com.epam.kirill_barykin.java.lesson2.model.helicopters.AtlasOrix;

import java.io.*;

/**
 * Author: Kirill Barykin
 *
 * Добавьте для иерархии объектов из 2 задачи возможность
 * сериализоваться/десериализоваться. Однако все численные поля не
 * должны подвергаться этой процедуре. Предоставьте тестовый код, в
 * котором вы создаете несколько экземпляров различных классов этой
 * иерархии и успешно выполняете операцию
 * сериализации/десереализации.
 *
 */
public class SerializationThing {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        SerializationThing serializationThing = new SerializationThing();

        serializationThing.startApplication();
    }

    private void startApplication() throws IOException, ClassNotFoundException {
        A380 a380 = new A380();
        Falcon falcon = new Falcon();
        AtlasOrix atlasOrix = new AtlasOrix();

        serialize(a380);
        deserialize(a380);

        serialize(falcon);
        deserialize(falcon);

        serialize(atlasOrix);
        deserialize(atlasOrix);
    }

    /**
     * Serializes objects to .txt file with unique names.txt (matches model name)
     *
     * @param aircraft - serializable object
     * @throws IOException
     */
    private void serialize(BaseAircraft aircraft) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(aircraft.getName() + ".txt");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

        objectOutputStream.writeObject(aircraft);
        objectOutputStream.close();
    }

    /**
     * Deserializes objects from .txt file and output its model name
     *
     * @param aircraft - serializable object
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void deserialize(BaseAircraft aircraft) throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(aircraft.getName() + ".txt");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        BaseAircraft baseAircraft = (BaseAircraft) objectInputStream.readObject();
        System.out.println(baseAircraft.getModel());
    }

}
