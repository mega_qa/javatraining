package com.epam.kirill_barykin.java.lesson5.task2;

import java.io.File;
import java.util.*;

/**
 * Author: Kirill Barykin
 *
 * Напишите утилиту DiskAnalyzer командной строки, которая на вход
 * принимает путь (например C:\) и номер функции, корректно
 * обрабатывает некорректные пути или номера функций. Утилита выводит
 * в файл результаты своей работы. Программа должна работать для диска
 * C: вашей рабочей машины. Поддерживаются следующие функции:
 * 1. Поиск имени файла с максимальным количеством букв ‘s’ в имени,
 * вывод пути к нему.
 * 2. Top-5 файлов с самым большим размером
 * 3. Средний размер файла в указанной директории или любой ее
 * поддиректории
 * 4. Количество файлов и папок разбитое по первым буквам алфавита
 * (например на букву A – начинается 100 000 файлов и 200 папок)
 *
 */
public class DiskAnalyzer {

    public static void main(String[] args) {
        DiskAnalyzer diskAnalyzer = new DiskAnalyzer();

        diskAnalyzer.startApplication();
    }

    public void startApplication() {
        DiskAnalyzer diskAnalyzer = new DiskAnalyzer();

        diskAnalyzer.functionChoice();
    }

    /**
     * Choosing a function number to execute
     * (I can move it to "startApplication" but I think it's actually not right)
     */
    public void functionChoice() {
        DiskAnalyzer diskAnalyzer = new DiskAnalyzer();
        ConsoleInput consoleInput = new ConsoleInput();
        File coreDirectory;

        System.out.println("Where will we look? In root directory? (y | n)");
        if (!consoleInput.chooseYesOrNo()) {
            Scanner scanner = new Scanner(System.in);
            do {
                System.out.println("Enter the path:");
                coreDirectory = new File(scanner.nextLine());
                if (!coreDirectory.exists()) {
                    System.out.println("You entered the wrong path. Try again.");
                }
            } while (!coreDirectory.exists());
        } else {
            coreDirectory = new File(System.getProperty("user.home"));
        }

        System.out.println("Which number of function do you want to choose ( 1 | 2 | 3 | 4 )?");

        switch (consoleInput.chooseFunctionNumber()) {
            case 1:
                diskAnalyzer.maxSCharsInFileName(coreDirectory);
                break;
            case 2:
                diskAnalyzer.topFiveWithLargestSize(coreDirectory);
                break;
            case 3:
                diskAnalyzer.averageFileSize(coreDirectory);
                break;
            case 4:
                diskAnalyzer.filesAndFoldersByFirstLetters(coreDirectory);
                break;
        }

        System.out.println("Do you want to execute another function (y | n) ?");

        if (consoleInput.chooseYesOrNo()) {
            diskAnalyzer.functionChoice();
        }
    }

    /**
     * (1)
     * Search for the file name with the maximum number of letters ‘s’ in
     * name and conclusion of the path to it.
     */
    public void maxSCharsInFileName(File coreDirectory) {
        long maxNumberOfSChars = 0;
        String maxSCharsName = "null";

        if (coreDirectory.isDirectory()) {
            File[] files = coreDirectory.listFiles();

            if (files != null) {
                long numberOfSChars = 0;
                for (File file : files) {
                    if (file.isFile()) { //(file.isFile() && !file.isHidden()) - to search only visible files
                        numberOfSChars = file.getName().toLowerCase().chars().filter(ch -> ch == 's').count();
                        if (numberOfSChars > maxNumberOfSChars) {
                            maxNumberOfSChars = numberOfSChars;
                            maxSCharsName = file.getAbsolutePath();
                        }
                    }
                }
            }
            System.out.println(maxSCharsName);
        }
    }


    /**
     * (2)
     * Top 5 files with largest sizes
     */
    public void topFiveWithLargestSize(File coreDirectory) {
        Map<File, Integer> fileMap = new HashMap<>();

        if (coreDirectory.isDirectory()) {
            File[] files = coreDirectory.listFiles();

            if (files != null) {
                for (File file : files) {
                    if (file.isFile()) { //(file.isFile() && !file.isHidden()) - to search only visible files
                        fileMap.put(file, (int) file.length());
                    }
                }
                fileMap.entrySet().stream()
                        .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                        .limit(5)
                        .forEach(System.out::println);
            }
        }
    }

    /**
     * (3)
     * The average file size in the specified directory or any
     * subdirectories
     */
    public void averageFileSize(File coreDirectory) {
        int numberOfFiles;
        int sumOfSizes;

        List<Integer> fileLengthList = new ArrayList<>();
        search(coreDirectory, fileLengthList);

        sumOfSizes = fileLengthList.stream().mapToInt(Integer::intValue).sum();
        numberOfFiles = fileLengthList.size();

        System.out.println("The average file size in chosen directory: " + sumOfSizes / numberOfFiles);

    }

    /**
     * Search file sizes in the specified directory
     *
     * @param coreDirectory
     * @param fileLengthList - list of file sizes
     */
    public static void search(File coreDirectory, List<Integer> fileLengthList) {
        if (coreDirectory != null) {
            for (File f : coreDirectory.listFiles()) {
                if (f.isDirectory()) {
                    search(f, fileLengthList);
                }
                if (f.isFile()) {
                    fileLengthList.add((int) f.length());
                }
            }
        }
    }

    /**
     * (4)
     * Number of files and folders sorted by first letters alphabet
     */
    public void filesAndFoldersByFirstLetters(File coreDirectory) {
        Map<Character, Integer> filesMap = new HashMap<>();

        if (coreDirectory.isDirectory()) {
            File[] files = coreDirectory.listFiles();

            if (files != null) {
                for (File file : files) {
                    char letter = file.getName().toUpperCase().charAt(0);
                    if (Character.isAlphabetic(letter)) {
                        filesMap.merge(letter, 1, Integer::sum);
                    }
                }
                System.out.println(filesMap);
            } else {
                System.out.println("There is no files here");
            }
        }
    }
}
