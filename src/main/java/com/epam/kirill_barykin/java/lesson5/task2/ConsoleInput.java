package com.epam.kirill_barykin.java.lesson5.task2;

import java.util.Scanner;

public class ConsoleInput {

    /**
     * User selectable action
     */
    public int chooseFunctionNumber() {
        Scanner scanner = new Scanner(System.in);

        int input;

        do {
            while (!scanner.hasNextInt()) {
                System.out.println("It is not a number. Try again: ");
                scanner.next();
            }
            input = scanner.nextInt();
            if (input <= 0)
                System.out.println("The number must be greater than zero");
            if (input > 4)
                System.out.println("There is only 4 functions. The number can't be greater than 4");
        } while (input <= 0 || input > 4);

        return input;
    }

    /**
     * Choose "Yes" or "No"
     *
     * @return chosen answer.
     */
    public Boolean chooseYesOrNo() {
        Scanner scanner = new Scanner(System.in);
        boolean answer = false;
        boolean correctValue = false;

        do {
            switch (scanner.nextLine().toLowerCase()) {
                case "yes":
                case "y":
                    answer = true;
                    correctValue = true;
                    break;
                case "no":
                case "n":
                    answer = false;
                    correctValue = true;
                    break;
                default:
                    System.out.println("You need to insert just only \"y\" or \"n\"");
            }
        } while (!correctValue);

        return answer;
    }
}
