package com.epam.kirill_barykin.java.lesson2.service;

import com.epam.kirill_barykin.java.lesson2.model.Aircraft;

public class ConsoleOutput {

    /**
     * Output aircraft to console.
     *
     * @param aircraft - core array with aircraft.
     */
    public void outputAircraft(Aircraft[] aircraft) {
        int index = 1;

        if ((aircraft!=null) && (aircraft.length > 0)) {
            System.out.println("You may use this aircraft: ");
            for (int i = 0; i < aircraft.length; i++) {
                if (aircraft[i] != null) {
                    System.out.println(index + ") " + aircraft[i].getName());
                    index++;
                }
            }

        } else {
            System.out.println("Sorry, there is no aircraft satisfying your parameters in airline.");
        }
    }
}
