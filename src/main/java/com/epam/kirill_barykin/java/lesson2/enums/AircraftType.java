package com.epam.kirill_barykin.java.lesson2.enums;

public enum AircraftType {
    HELICOPTER, DRONE, AIRPLANE
}
