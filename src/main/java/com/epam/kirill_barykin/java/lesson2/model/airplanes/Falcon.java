package com.epam.kirill_barykin.java.lesson2.model.airplanes;

import com.epam.kirill_barykin.java.lesson2.enums.AircraftType;
import com.epam.kirill_barykin.java.lesson2.model.BaseAircraft;

public class Falcon extends BaseAircraft {

    public Falcon(int maximumLoad, int maximumNumberOfPassengers, int rangeOfFlight) {
        super(maximumLoad, maximumNumberOfPassengers, rangeOfFlight, 1, "Falcon 3000", AircraftType.AIRPLANE);

    }
    public Falcon() {
        super(6000, 10, 7400, 1, "Falcon 3000", AircraftType.AIRPLANE);
    }



}
