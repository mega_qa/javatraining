package com.epam.kirill_barykin.java.lesson2.model;


public interface Aircraft {


    /**
     * Get range of flight
     *
     * @return range of flight
     */
    int getRangeOfFlight();



    /**
     * Get maximum number of passengers
     *
     * @return maximum number of passengers
     */
    int getMaximumNumberOfPassengers();



    /**
     * Get maximum load
     *
     * @return - maximum load
     */
    int getMaximumLoad();

    /**
     * Get full name for aircraft
     *
     * @return - aircraft full name
     */
    String getName();

    boolean isPassengersTransitional ();

}

