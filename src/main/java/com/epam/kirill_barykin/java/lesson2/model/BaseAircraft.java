package com.epam.kirill_barykin.java.lesson2.model;

import com.epam.kirill_barykin.java.lesson2.enums.AircraftType;
import com.epam.kirill_barykin.java.lesson7.annotations.MinDistance;
import com.epam.kirill_barykin.java.lesson7.annotations.NotNullField;
import com.epam.kirill_barykin.java.lesson7.annotations.NullableTypes;
import com.epam.kirill_barykin.java.lesson7.annotations.Range;

import java.io.Serializable;

public abstract class BaseAircraft implements Aircraft, Serializable {
    @Range(max = 500)
    private transient int maximumLoad;
    @Range(min = 10, max = 1000)
    private transient int maximumNumberOfPassengers;
    @MinDistance(30)
    private transient int rangeOfFlight;
    private transient int takeoffDistance;
    @NotNullField
    private String model;

    @NotNullField
    private AircraftType type;

    public BaseAircraft(int maximumLoad, int maximumNumberOfPassengers, int rangeOfFlight, int takeoffDistance, String model, AircraftType type) {
        this.maximumLoad = maximumLoad;
        this.maximumNumberOfPassengers = maximumNumberOfPassengers;
        this.rangeOfFlight = takeoffDistance > 0 ? rangeOfFlight - takeoffDistance : rangeOfFlight;
        this.takeoffDistance = takeoffDistance;
        this.model = model;
        this.type = type;
    }

    @Range(max = 1000)
    public int getMaximumLoad() {
        return maximumLoad;
    }

    public int getMaximumNumberOfPassengers() {
        return maximumNumberOfPassengers;
    }

    public int getRangeOfFlight() {
        return rangeOfFlight;
    }

    @NullableTypes({AircraftType.DRONE, AircraftType.HELICOPTER})
    public int getTakeoffDistance() {
        return takeoffDistance;
    }

    public String getModel() {
        return model;
    }

    public AircraftType getType() {
        return type;
    }

    @Override
    public String getName() {
        return String.format("%1$s - %2$s", type.name(), model);
    }

    @Override
    public boolean isPassengersTransitional() {
        return maximumNumberOfPassengers > 0;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setType(AircraftType type) {
        this.type = type;
    }
}
