package com.epam.kirill_barykin.java.lesson2.model.helicopters;

import com.epam.kirill_barykin.java.lesson2.enums.AircraftType;
import com.epam.kirill_barykin.java.lesson2.model.BaseAircraft;

public class Mi54 extends BaseAircraft {

     public Mi54(int maximumLoad, int rangeOfFlight) {
            super(maximumLoad, 0, rangeOfFlight, 0, "Mi-54", AircraftType.HELICOPTER);
        }

    public Mi54() {
            super(4500, 12, 700, 0, "Mi-54", AircraftType.HELICOPTER);
        }
}
