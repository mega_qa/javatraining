package com.epam.kirill_barykin.java.lesson2.model;


import com.epam.kirill_barykin.java.lesson2.service.ConsoleInput;
import com.epam.kirill_barykin.java.lesson3.exeptions.EmptyArrayException;
import com.epam.kirill_barykin.java.lesson3.exeptions.MaximumLoadException;
import com.epam.kirill_barykin.java.lesson3.exeptions.MaximumNumberOfPassengersException;
import com.epam.kirill_barykin.java.lesson3.exeptions.RangeOfFlightException;

import java.util.Arrays;

public class Airline {
    private final String emptyArrayExceptionMessage = "Aircraft array is empty";
    private Aircraft[] aircraft;

    public Airline(Aircraft[] aircraft) {
        this.aircraft = aircraft;
    }


    public Aircraft[] getAircraft() {
        return aircraft;
    }

    /**
     * Sorting aircraft by parameters in filter.
     *
     * @param aircraftFilter - filter with required parameters.
     * @return array with sorted aircraft.
     */
    public Aircraft[] getAircraft(AircraftFilter aircraftFilter) {

        int index = 0;
        if(!isValid(aircraftFilter)){
            return new Aircraft[0];
        }
        Aircraft[] filteredAircraft = new Aircraft[aircraft.length];
        for (int i = 0; i < filteredAircraft.length; i++) {
            if ((aircraft[i].getMaximumNumberOfPassengers() >= aircraftFilter.getMaximumNumberOfPassengers()) && (aircraft[i].getMaximumLoad() >= aircraftFilter.getMaximumLoad()) && (aircraft[i].getRangeOfFlight() >= aircraftFilter.getRangeOfFlight())) {
                filteredAircraft[i] = aircraft[i];
                index++;
            }
        }

        if (index > 0) {
            return filteredAircraft;
        } else {
            return new Aircraft[0];
        }

    }

    /**
     * Checking validity of filter
     *
     * @param aircraftFilter - core filter
     * @return - TRUE if filter is valid | FALSE if filter is not valid
     */
    private boolean isValid(AircraftFilter aircraftFilter) throws MaximumLoadException, MaximumNumberOfPassengersException, RangeOfFlightException{
        if (aircraftFilter.getMaximumLoad()<0){
            throw new MaximumLoadException();
        }
        if (aircraftFilter.getMaximumNumberOfPassengers()<0){
            throw new MaximumNumberOfPassengersException();
        }
        if (aircraftFilter.getRangeOfFlight()<=0){
            throw new RangeOfFlightException();
        }
        return true;
    }

    /**
     * Sort Aircraft by flight range
     *
     * @param coreArray - core array with Aircraft objects
     * @return Aircraft array, sorted by flight range and ordered by DESC
     */
    public Aircraft[] sortMerge(Aircraft[] coreArray) throws EmptyArrayException {

        if (coreArray.length==0){
            throw new EmptyArrayException(emptyArrayExceptionMessage);
        }

        int length = coreArray.length;
        if (length < 2) return coreArray;
        int middle = length / 2;
        return mergeFlightRange(sortMerge(Arrays.copyOfRange(coreArray, 0, middle)),
                sortMerge(Arrays.copyOfRange(coreArray, middle, length)));
    }

    /**
     * Sort Aircraft by flight range and order by DESC
     *
     * @param arrayA - first half of core array
     * @param arrayB - second half of core array
     * @return array with sorted Objects
     */
    private Aircraft[] mergeFlightRange(Aircraft[] arrayA, Aircraft[] arrayB) {

        int lengthA = arrayA.length, lengthB = arrayB.length;

        int a = 0, b = 0, len = lengthA + lengthB;
        Aircraft[] result = new Aircraft[len];

        for (int i = 0; i < len; i++) {
            if (b < lengthB && a < lengthA) {
                if (arrayA[a].getRangeOfFlight() < arrayB[b].getRangeOfFlight()) {
                    result[i] = arrayB[b++];
                } else
                    result[i] = arrayA[a++];
            } else if (b < lengthB) {
                result[i] = arrayB[b++];
            } else {
                result[i] = arrayA[a++];
            }
        }
        return result;
    }

    /**
     * Display aircraft with chosen params
     *
     * @param aircraftFilter - filter with params
     */
    public void displayAircraftSatisfyingParams(AircraftFilter aircraftFilter) {
        int index = 0;

        for (int i = 0; i < aircraft.length; i++) {
            if ((aircraft[i].getMaximumNumberOfPassengers() >= aircraftFilter.getMaximumNumberOfPassengers()) && (aircraft[i].getMaximumLoad() >= aircraftFilter.getMaximumLoad()) && (aircraft[i].getRangeOfFlight() >= aircraftFilter.getRangeOfFlight())) {
                index++;
            }

        }
        if (index > 0) {
            System.out.println("You may use this aircraft: ");
            for (int i = 0; i < aircraft.length; i++) {
                if ((aircraft[i].getMaximumNumberOfPassengers() >= aircraftFilter.getMaximumNumberOfPassengers()) && (aircraft[i].getMaximumLoad() >= aircraftFilter.getMaximumLoad()) && (aircraft[i].getRangeOfFlight() >= aircraftFilter.getRangeOfFlight())) {
                    System.out.println(aircraft[i].getName());
                }

            }
        } else {
            System.out.println("Sorry, there is no aircraft satisfying your parameters in airline.");
        }
    }

    /**
     * Sort array with units of airline's aircraft by range of flight
     * and output it on console.
     */
    public void sortAircraftByFlightRange() throws EmptyArrayException {
        System.out.println("Sorted by flight range:");

        Aircraft[] sortedAircraft = sortMerge(aircraft);
        for (int i = 0; i < sortedAircraft.length; i++) {
            System.out.println("[" + (i + 1) + ". (" + sortedAircraft[i].getRangeOfFlight() + ")" + "]: " + sortedAircraft[i].getName());
        }
    }

    /**
     * Calculate total payload of airline's aircraft.
     *
     * @return
     */
    public int totalPayload() {
        int totalLoad = 0;

        for (int i = 0; i < aircraft.length; i++) {
            totalLoad += this.aircraft[i].getMaximumLoad();
        }

        return totalLoad;
    }

    /**
     * Calculate total capacity (maximum number of people) of airline's aircraft.
     *
     * @return total capacity.
     */
    public int totalCapacity() {
        int totalCapacity = 0;

        for (int i = 0; i < aircraft.length; i++) {
            totalCapacity += aircraft[i].getMaximumNumberOfPassengers();
        }
        return totalCapacity;
    }

    /**
     * Choose filtering params
     *
     * @return filter with params
     */
    public AircraftFilter chooseFindingParams() {
        AircraftFilter aircraftFilter = new AircraftFilter();
        ConsoleInput consoleInput = new ConsoleInput();

        System.out.println("Do you need to transport some people using our airline?\n(y/n): ");
        switch (consoleInput.chooseYesOrNo()) {
            case "y":
                System.out.println("How many people do you need to transport?\nNumber of people: ");
                aircraftFilter.setMaximumNumberOfPassengers(consoleInput.inputNumber());
                break;
            case "n":
                aircraftFilter.setMaximumNumberOfPassengers(0);
        }

        System.out.println("Do you need to transport some cargo using our airline?\n(y/n): ");
        switch (consoleInput.chooseYesOrNo()) {
            case "y":
                System.out.println("How many kilos do you need to transport?\nKilograms: ");
                aircraftFilter.setMaximumLoad(consoleInput.inputNumber() + (aircraftFilter.getMaximumNumberOfPassengers() * 70));
                break;
            case "n":
                aircraftFilter.setMaximumLoad(aircraftFilter.getMaximumNumberOfPassengers() * 70);
        }

        System.out.println("How far is the flight planned?\nDistance:");
        aircraftFilter.setRangeOfFlight(consoleInput.inputNumber());

        return aircraftFilter;
    }


}
