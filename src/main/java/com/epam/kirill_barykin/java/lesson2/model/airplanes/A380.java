package com.epam.kirill_barykin.java.lesson2.model.airplanes;

import com.epam.kirill_barykin.java.lesson2.model.BaseAircraft;
import com.epam.kirill_barykin.java.lesson2.enums.AircraftType;

public class A380 extends BaseAircraft {

    public A380(int maximumLoad, int maximumNumberOfPassengers, int rangeOfFlight) {
        super(maximumLoad, maximumNumberOfPassengers, rangeOfFlight, 2, "Airbus A380", AircraftType.AIRPLANE);
    }

    public A380() {
        super(145000, 853, 15200, 2, "Airbus A380", AircraftType.AIRPLANE);

    }
}
