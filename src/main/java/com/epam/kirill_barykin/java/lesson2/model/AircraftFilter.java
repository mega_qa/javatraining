package com.epam.kirill_barykin.java.lesson2.model;

public class AircraftFilter {
    private int maximumLoad;
    private int maximumNumberOfPassengers;
    private int rangeOfFlight;

    public AircraftFilter setMaximumLoad(int maximumLoad) {
        this.maximumLoad = maximumLoad;
        return this;
    }

    public AircraftFilter setMaximumNumberOfPassengers(int maximumNumberOfPassengers) {
        this.maximumNumberOfPassengers = maximumNumberOfPassengers;
        return this;
    }

    public AircraftFilter setRangeOfFlight(int rangeOfFlight) {
        this.rangeOfFlight = rangeOfFlight;
        return this;
    }

    public int getMaximumLoad() {
        return maximumLoad;
    }

    public int getMaximumNumberOfPassengers() {
        return maximumNumberOfPassengers;
    }

    public int getRangeOfFlight() {
        return rangeOfFlight;
    }
}
