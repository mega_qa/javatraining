package com.epam.kirill_barykin.java.lesson2;

import com.epam.kirill_barykin.java.lesson2.model.Aircraft;
import com.epam.kirill_barykin.java.lesson2.model.airplanes.A380;
import com.epam.kirill_barykin.java.lesson2.model.airplanes.Falcon;
import com.epam.kirill_barykin.java.lesson2.model.drones.MavicAir;
import com.epam.kirill_barykin.java.lesson2.model.helicopters.AtlasOrix;
import com.epam.kirill_barykin.java.lesson2.model.helicopters.Mi54;
import com.epam.kirill_barykin.java.lesson2.model.Airline;
import com.epam.kirill_barykin.java.lesson2.service.ConsoleOutput;
import com.epam.kirill_barykin.java.lesson3.exeptions.EmptyArrayException;

/**
 * Author: Kirill Barykin
 *
 * Задание (Вариант №3):
 * Авиакомпания. Определить иерархию самолетов, вертолетов,
 * квадракоптеров и т.д. Создать авиакомпанию. Посчитать общую
 * вместимость и грузоподъемность. Провести сортировку летных
 * средств компании по дальности полета. Найти самолет в компании,
 * соответствующий заданному диапазону параметров.
 */

public class FindVehiclesByParams {
    public static void main(String[] args) {
        FindVehiclesByParams findVehiclesByParams = new FindVehiclesByParams();

        findVehiclesByParams.startApplication();
    }

    public void startApplication() {

        Aircraft[] aircraft = new Aircraft[]{
                new Falcon(),
                new MavicAir(),
                new Mi54(),
                new A380(),
                new AtlasOrix()
        };

        Airline airline = new Airline(aircraft);
        ConsoleOutput consoleOutput = new ConsoleOutput();

        try {
            airline.sortAircraftByFlightRange();
        } catch (EmptyArrayException e) {
            e.printStackTrace();
        }
        System.out.println("Total capacity: " + airline.totalCapacity());
        System.out.println("Total load: " + airline.totalPayload());

        consoleOutput.outputAircraft(airline.getAircraft(airline.chooseFindingParams()));
    }
}
