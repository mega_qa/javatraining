package com.epam.kirill_barykin.java.lesson2.model.drones;

import com.epam.kirill_barykin.java.lesson2.model.BaseAircraft;
import com.epam.kirill_barykin.java.lesson2.enums.AircraftType;

public class MavicAir extends BaseAircraft {

    public MavicAir(int maximumLoad, int rangeOfFlight) {
        super(maximumLoad, 0, rangeOfFlight, 0, "DJI Mavic Air", AircraftType.DRONE);
    }
    public MavicAir() {
        super(10, 0, 20, 0, "DJI Mavic Air", AircraftType.DRONE);
    }
}
