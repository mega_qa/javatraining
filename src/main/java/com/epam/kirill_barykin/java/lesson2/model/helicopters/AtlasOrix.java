package com.epam.kirill_barykin.java.lesson2.model.helicopters;

import com.epam.kirill_barykin.java.lesson2.enums.AircraftType;
import com.epam.kirill_barykin.java.lesson2.model.BaseAircraft;

public class AtlasOrix extends BaseAircraft {

    public AtlasOrix(int maximumLoad, int rangeOfFlight) {
        super(maximumLoad, 0, rangeOfFlight, 0, "DJI Mavic Air", AircraftType.HELICOPTER);
    }

    public AtlasOrix() {
        super(8000, 23, 2000, 0, "Atlas Oryx", AircraftType.HELICOPTER);
    }
}
