package com.epam.kirill_barykin.java.lesson2.service;

import java.util.Scanner;

public class ConsoleInput {

    /**
     * Input number
     *
     * @return inputted number
     */
    public int inputNumber() {

        Scanner scanner = new Scanner(System.in);

        int input;

        do {
            while (!scanner.hasNextInt()) {
                System.out.println("It is not a number. Try again: ");
                scanner.next();
            }
            input = scanner.nextInt();
            if (input <= 0)
                System.out.println("The number must be greater than zero");
        } while (input <= 0);

        return input;
    }

    /**
     * Choose "Yes" or "No"
     *
     * @return chosen answer (it could be boolean, but I made it for possibility of code expansion).
     */
    public String chooseYesOrNo() {
        Scanner scanner = new Scanner(System.in);
        String answer = null;
        boolean correctValue = false;

        do {
            switch (scanner.nextLine().toLowerCase()) {
                case "yes":
                case "y":
                    answer = "y";
                    correctValue = true;
                    break;
                case "no":
                case "n":
                    answer = "n";
                    correctValue = true;
                    break;
                default:
                    System.out.println("You need to insert just only \"y\" or \"n\"");
            }
        } while (!correctValue);

        return answer;
    }
}
